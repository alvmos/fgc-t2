﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace T2
{
    public partial class HarrisDialog : Form
    {
        public HarrisDialog()
        {
            InitializeComponent();
        }

        private void acceptButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        public int Radius { get { return (int)radiusControl.Value; } }

        public float K { get { return (float)kControl.Value; } }

        public float Covariance { get { return (float)covarianceControl.Value; } }

        public float Threshold { get { return (float)thresholdControl.Value; } }
    }
}
