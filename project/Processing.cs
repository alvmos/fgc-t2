﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace T2
{
    public struct Corner
    {
        public Point Position;
        public float Response;

        public Corner(Point p, float response)
        {
            Position = p;
            Response = response;
        }

        public override string ToString()
        {
            return Math.Round(Response, 2) + " (" + Position.X + ", " + Position.Y + ")";
        }
    }

    static class Processing
    {
        public static readonly float[,] Gx = { { 1, 2, 1 }, { 0, 0, 0 }, { -1, -2, -1 } };
        public static readonly float[,] Gy = { { 1, 0, -1 }, { 2, 0, -2 }, { 1, 0, -1 } };

        public static Bitmap GetBitmapFromIntensity(float[,] intensity)
        {
            Bitmap image = new Bitmap(intensity.GetLength(0), intensity.GetLength(1));
            for (int i = 0; i < image.Width; i++)
                for (int j = 0; j < image.Height; j++)
                    image.SetPixel(i, j, Color.FromArgb((int)(intensity[i, j] * 255), (int)(intensity[i, j] * 255), (int)(intensity[i, j] * 255)));
            return image;
        }

        public static float[,] Convolution(float[,] matrix, float[,] kernel)
        {
            float[,] result = new float[matrix.GetLength(0), matrix.GetLength(1)];
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (i == 0 || i == matrix.GetLength(0) - 1 || j == 0 || j == matrix.GetLength(1) - 1)
                        result[i, j] = 0;
                    else
                    {
                        result[i, j] =
                            kernel[0, 0] * matrix[i - 1, j - 1] +
                            kernel[0, 1] * matrix[i - 1, j] +
                            kernel[0, 2] * matrix[i - 1, j + 1] +
                            kernel[1, 0] * matrix[i, j - 1] +
                            kernel[1, 1] * matrix[i, j] +
                            kernel[1, 2] * matrix[i, j + 1] +
                            kernel[2, 0] * matrix[i + 1, j - 1] +
                            kernel[2, 1] * matrix[i + 1, j] +
                            kernel[2, 2] * matrix[i + 1, j + 1];
                    }
                }
            }
            return result;
        }

        public static float[,] ImageToMatrix(Bitmap image)
        {
            float[,] result = new float[image.Width, image.Height];
            for (int i = 0; i < image.Width; i++)
                for (int j = 0; j < image.Height; j++)
                    result[i, j] = image.GetPixel(i, j).GetBrightness();
            return result;
        }

        public static Bitmap MatrixToImage(float[,] matrix)
        {
            float[,] I = Normalize(matrix);
            Bitmap bmp = new Bitmap(matrix.GetLength(0), matrix.GetLength(1));
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    int value = (int)(255 * I[i, j]);
                    bmp.SetPixel(i, j, Color.FromArgb(value, value, value));
                }
            }
            return bmp;
        }

        public static float[,] Normalize(float[,] matrix)
        {
            float min = float.MaxValue, max = float.MinValue;
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (matrix[i,j]<min)
                        min = matrix[i, j];
                    if (matrix[i,j]>max)
                        max = matrix[i, j];
                }
            }
            float deltha = max - min + 0.0001f;
            float[,] result = new float[matrix.GetLength(0), matrix.GetLength(1)];
            for (int i = 0; i < matrix.GetLength(0); i++)
                for (int j = 0; j < matrix.GetLength(1); j++)
                    result[i, j] = (matrix[i, j] - min) / deltha;
            return result;
        }

        public static float[,] Product(float[,] m1, float[,] m2)
        {
            float[,] result = new float[m1.GetLength(0), m1.GetLength(1)];
            for (int i = 0; i < m1.GetLength(0); i++)
            {
                for (int j = 0; j < m1.GetLength(1); j++)
                {
                    result[i, j] = m1[i, j] * m2[i, j];
                }
            }
            return result;
        }

        public static float[,] GetGaussianKernel(int length, float weight)
        {
            float[,] Kernel = new float[length, length];
            float sumTotal = 0;


            int kernelRadius = length / 2;
            float distance = 0;


            float calculatedEuler = (float)(1.0 /(2.0 * Math.PI * Math.Pow(weight, 2)));


            for (int filterY = -kernelRadius;
                 filterY <= kernelRadius; filterY++)
            {
                for (int filterX = -kernelRadius;
                    filterX <= kernelRadius; filterX++)
                {
                    distance = ((filterX * filterX) + (filterY * filterY)) / (2 * (weight * weight));


                    Kernel[filterY + kernelRadius,
                           filterX + kernelRadius] =
                           calculatedEuler * (float)Math.Exp(-distance);


                    sumTotal += Kernel[filterY + kernelRadius,
                                       filterX + kernelRadius];
                }
            }


            for (int y = 0; y < length; y++)
            {
                for (int x = 0; x < length; x++)
                {
                    Kernel[y, x] = Kernel[y, x] * (1 / sumTotal);
                }
            }


            return Kernel;
        }

        public static float[,] CalculateResponse(float[,] Sxx, float[,] Sxy, float[,] Syy, float K)
        {
            float[,] result = new float[Sxx.GetLength(0), Sxx.GetLength(1)];
            for (int i = 0; i < result.GetLength(0); i++)
            {
                for (int j = 0; j < result.GetLength(1); j++)
                {
                    result[i, j] = (Sxx[i, j] * Syy[i, j] - Sxy[i, j] * Sxy[i, j]) - K * (Sxx[i, j] + Syy[i, j]) * (Sxx[i, j] + Syy[i, j]);
                }
            }
            return result;
        }

        public static float[,] Threshold(float[,] matrix, float min)
        {
            float[,] result = new float[matrix.GetLength(0), matrix.GetLength(1)];
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    result[i, j] = matrix[i, j] < min ? 0 : matrix[i, j];
                }
            }
            return result;
        }

        public static Point[] NonMaximumSuppression(float[,] matrix, int radius)
        {
            List<Point> points = new List<Point>();
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    bool isMax = true;
                    for (int k = -radius; k <= radius; k++)
                    {
                        for (int l = -radius; l <= radius; l++)
                        {
                            if (i + k >= 0 && i + k < matrix.GetLength(0) && j + l >= 0 && j + l < matrix.GetLength(1) && matrix[i, j] < matrix[i + k, j + l])
                            {
                                isMax = false;
                                break;
                            }
                        }
                        if (!isMax) break;
                    }
                    if (isMax && matrix[i,j] > 0)
                        points.Add(new Point(i, j));
                }
            }
            return points.ToArray();
        }

        public static Corner[] SelectCorners(float[,] responses, float threshold, int radius)
        {
            List<Corner> corners = new List<Corner>();
            for (int i = 0; i < responses.GetLength(0); i++)
            {
                for (int j = 0; j < responses.GetLength(1); j++)
                {
                    // Threshold
                    if (responses[i, j] >= threshold)
                    {
                        // Non-Maximum Suppression
                        bool isMax = true;
                        for (int k = -radius; k <= radius; k++)
                        {
                            for (int l = -radius; l <= radius; l++)
                            {
                                if ((i + k >= 0) && (i + k < responses.GetLength(0)) && 
                                    (j + l >= 0) && (j + l < responses.GetLength(1)) && 
                                    responses[i, j] < responses[i + k, j + l])
                                {
                                    isMax = false;
                                    break;
                                }
                            }
                            if (!isMax) break;
                        }
                        if (isMax)
                            corners.Add(new Corner(new Point(i, j), responses[i, j]));
                    }
                }
            }
            return corners.ToArray();
        }
    }
}
