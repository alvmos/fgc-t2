﻿namespace T2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.openButton = new System.Windows.Forms.Button();
            this.harrisButton = new System.Windows.Forms.Button();
            this.optionsGroupBox = new System.Windows.Forms.GroupBox();
            this.yDerivateRadioButton = new System.Windows.Forms.RadioButton();
            this.xDerivateRadioButton = new System.Windows.Forms.RadioButton();
            this.smoothedDerivatesProductRadioButton = new System.Windows.Forms.RadioButton();
            this.smoothedSquaredYDerivateRadioButton = new System.Windows.Forms.RadioButton();
            this.smoothedSquaredXDerivateRadioButton = new System.Windows.Forms.RadioButton();
            this.derivatesProductRadioButton = new System.Windows.Forms.RadioButton();
            this.squaredYDerivateRadioButton = new System.Windows.Forms.RadioButton();
            this.squaredXDerivateRadioButton = new System.Windows.Forms.RadioButton();
            this.cornerResponseRadioButton = new System.Windows.Forms.RadioButton();
            this.detectedCornersRadioButton = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.listBox = new System.Windows.Forms.ListBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.optionsGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox
            // 
            this.pictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pictureBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox.Location = new System.Drawing.Point(12, 12);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(878, 633);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox.TabIndex = 0;
            this.pictureBox.TabStop = false;
            this.pictureBox.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox_Paint);
            this.pictureBox.Resize += new System.EventHandler(this.pictureBox_Resize);
            // 
            // openFileDialog
            // 
            this.openFileDialog.Filter = "Image files|*.jpg;*.png;*.bmp";
            // 
            // openButton
            // 
            this.openButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.openButton.Location = new System.Drawing.Point(896, 12);
            this.openButton.Name = "openButton";
            this.openButton.Size = new System.Drawing.Size(67, 23);
            this.openButton.TabIndex = 1;
            this.openButton.Text = "Open";
            this.openButton.UseVisualStyleBackColor = true;
            this.openButton.Click += new System.EventHandler(this.openButton_Click);
            // 
            // harrisButton
            // 
            this.harrisButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.harrisButton.Enabled = false;
            this.harrisButton.Location = new System.Drawing.Point(969, 12);
            this.harrisButton.Name = "harrisButton";
            this.harrisButton.Size = new System.Drawing.Size(65, 23);
            this.harrisButton.TabIndex = 2;
            this.harrisButton.Text = "Harris";
            this.harrisButton.UseVisualStyleBackColor = true;
            this.harrisButton.Click += new System.EventHandler(this.harrisButton_Click);
            // 
            // optionsGroupBox
            // 
            this.optionsGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.optionsGroupBox.Controls.Add(this.yDerivateRadioButton);
            this.optionsGroupBox.Controls.Add(this.xDerivateRadioButton);
            this.optionsGroupBox.Controls.Add(this.smoothedDerivatesProductRadioButton);
            this.optionsGroupBox.Controls.Add(this.smoothedSquaredYDerivateRadioButton);
            this.optionsGroupBox.Controls.Add(this.smoothedSquaredXDerivateRadioButton);
            this.optionsGroupBox.Controls.Add(this.derivatesProductRadioButton);
            this.optionsGroupBox.Controls.Add(this.squaredYDerivateRadioButton);
            this.optionsGroupBox.Controls.Add(this.squaredXDerivateRadioButton);
            this.optionsGroupBox.Controls.Add(this.cornerResponseRadioButton);
            this.optionsGroupBox.Controls.Add(this.detectedCornersRadioButton);
            this.optionsGroupBox.Enabled = false;
            this.optionsGroupBox.Location = new System.Drawing.Point(896, 41);
            this.optionsGroupBox.Name = "optionsGroupBox";
            this.optionsGroupBox.Size = new System.Drawing.Size(141, 256);
            this.optionsGroupBox.TabIndex = 3;
            this.optionsGroupBox.TabStop = false;
            this.optionsGroupBox.Text = "Images";
            // 
            // yDerivateRadioButton
            // 
            this.yDerivateRadioButton.AutoSize = true;
            this.yDerivateRadioButton.Location = new System.Drawing.Point(7, 43);
            this.yDerivateRadioButton.Name = "yDerivateRadioButton";
            this.yDerivateRadioButton.Size = new System.Drawing.Size(75, 17);
            this.yDerivateRadioButton.TabIndex = 10;
            this.yDerivateRadioButton.TabStop = true;
            this.yDerivateRadioButton.Text = "Y Derivate";
            this.yDerivateRadioButton.UseVisualStyleBackColor = true;
            this.yDerivateRadioButton.CheckedChanged += new System.EventHandler(this.yDerivateRadioButton_CheckedChanged);
            // 
            // xDerivateRadioButton
            // 
            this.xDerivateRadioButton.AutoSize = true;
            this.xDerivateRadioButton.Location = new System.Drawing.Point(7, 20);
            this.xDerivateRadioButton.Name = "xDerivateRadioButton";
            this.xDerivateRadioButton.Size = new System.Drawing.Size(75, 17);
            this.xDerivateRadioButton.TabIndex = 9;
            this.xDerivateRadioButton.TabStop = true;
            this.xDerivateRadioButton.Text = "X Derivate";
            this.xDerivateRadioButton.UseVisualStyleBackColor = true;
            this.xDerivateRadioButton.CheckedChanged += new System.EventHandler(this.xDerivateRadioButton_CheckedChanged);
            // 
            // smoothedDerivatesProductRadioButton
            // 
            this.smoothedDerivatesProductRadioButton.AutoSize = true;
            this.smoothedDerivatesProductRadioButton.Location = new System.Drawing.Point(7, 181);
            this.smoothedDerivatesProductRadioButton.Name = "smoothedDerivatesProductRadioButton";
            this.smoothedDerivatesProductRadioButton.Size = new System.Drawing.Size(107, 17);
            this.smoothedDerivatesProductRadioButton.TabIndex = 7;
            this.smoothedDerivatesProductRadioButton.TabStop = true;
            this.smoothedDerivatesProductRadioButton.Text = "Smothed Product";
            this.smoothedDerivatesProductRadioButton.UseVisualStyleBackColor = true;
            this.smoothedDerivatesProductRadioButton.CheckedChanged += new System.EventHandler(this.smoothedDerivatesProductRadioButton_CheckedChanged);
            // 
            // smoothedSquaredYDerivateRadioButton
            // 
            this.smoothedSquaredYDerivateRadioButton.AutoSize = true;
            this.smoothedSquaredYDerivateRadioButton.Location = new System.Drawing.Point(7, 158);
            this.smoothedSquaredYDerivateRadioButton.Name = "smoothedSquaredYDerivateRadioButton";
            this.smoothedSquaredYDerivateRadioButton.Size = new System.Drawing.Size(120, 17);
            this.smoothedSquaredYDerivateRadioButton.TabIndex = 6;
            this.smoothedSquaredYDerivateRadioButton.TabStop = true;
            this.smoothedSquaredYDerivateRadioButton.Text = "Smothed Squared Y";
            this.smoothedSquaredYDerivateRadioButton.UseVisualStyleBackColor = true;
            this.smoothedSquaredYDerivateRadioButton.CheckedChanged += new System.EventHandler(this.smoothedSquaredYDerivateRadioButton_CheckedChanged);
            // 
            // smoothedSquaredXDerivateRadioButton
            // 
            this.smoothedSquaredXDerivateRadioButton.AutoSize = true;
            this.smoothedSquaredXDerivateRadioButton.Location = new System.Drawing.Point(7, 135);
            this.smoothedSquaredXDerivateRadioButton.Name = "smoothedSquaredXDerivateRadioButton";
            this.smoothedSquaredXDerivateRadioButton.Size = new System.Drawing.Size(120, 17);
            this.smoothedSquaredXDerivateRadioButton.TabIndex = 5;
            this.smoothedSquaredXDerivateRadioButton.TabStop = true;
            this.smoothedSquaredXDerivateRadioButton.Text = "Smothed Squared X";
            this.smoothedSquaredXDerivateRadioButton.UseVisualStyleBackColor = true;
            this.smoothedSquaredXDerivateRadioButton.CheckedChanged += new System.EventHandler(this.smoothedSquaredXDerivateRadioButton_CheckedChanged);
            // 
            // derivatesProductRadioButton
            // 
            this.derivatesProductRadioButton.AutoSize = true;
            this.derivatesProductRadioButton.Location = new System.Drawing.Point(7, 112);
            this.derivatesProductRadioButton.Name = "derivatesProductRadioButton";
            this.derivatesProductRadioButton.Size = new System.Drawing.Size(110, 17);
            this.derivatesProductRadioButton.TabIndex = 4;
            this.derivatesProductRadioButton.TabStop = true;
            this.derivatesProductRadioButton.Text = "Derivates Product";
            this.derivatesProductRadioButton.UseVisualStyleBackColor = true;
            this.derivatesProductRadioButton.CheckedChanged += new System.EventHandler(this.derivatesProductRadioButton_CheckedChanged);
            // 
            // squaredYDerivateRadioButton
            // 
            this.squaredYDerivateRadioButton.AutoSize = true;
            this.squaredYDerivateRadioButton.Location = new System.Drawing.Point(7, 89);
            this.squaredYDerivateRadioButton.Name = "squaredYDerivateRadioButton";
            this.squaredYDerivateRadioButton.Size = new System.Drawing.Size(118, 17);
            this.squaredYDerivateRadioButton.TabIndex = 3;
            this.squaredYDerivateRadioButton.TabStop = true;
            this.squaredYDerivateRadioButton.Text = "Squared Y Derivate";
            this.squaredYDerivateRadioButton.UseVisualStyleBackColor = true;
            this.squaredYDerivateRadioButton.CheckedChanged += new System.EventHandler(this.squaredYDerivateRadioButton_CheckedChanged);
            // 
            // squaredXDerivateRadioButton
            // 
            this.squaredXDerivateRadioButton.AutoSize = true;
            this.squaredXDerivateRadioButton.Location = new System.Drawing.Point(7, 66);
            this.squaredXDerivateRadioButton.Name = "squaredXDerivateRadioButton";
            this.squaredXDerivateRadioButton.Size = new System.Drawing.Size(118, 17);
            this.squaredXDerivateRadioButton.TabIndex = 2;
            this.squaredXDerivateRadioButton.TabStop = true;
            this.squaredXDerivateRadioButton.Text = "Squared X Derivate";
            this.squaredXDerivateRadioButton.UseVisualStyleBackColor = true;
            this.squaredXDerivateRadioButton.CheckedChanged += new System.EventHandler(this.squaredXDerivateRadioButton_CheckedChanged);
            // 
            // cornerResponseRadioButton
            // 
            this.cornerResponseRadioButton.AutoSize = true;
            this.cornerResponseRadioButton.Location = new System.Drawing.Point(7, 204);
            this.cornerResponseRadioButton.Name = "cornerResponseRadioButton";
            this.cornerResponseRadioButton.Size = new System.Drawing.Size(107, 17);
            this.cornerResponseRadioButton.TabIndex = 1;
            this.cornerResponseRadioButton.TabStop = true;
            this.cornerResponseRadioButton.Text = "Corner Response";
            this.cornerResponseRadioButton.UseVisualStyleBackColor = true;
            this.cornerResponseRadioButton.CheckedChanged += new System.EventHandler(this.cornerResponseRadioButton_CheckedChanged);
            // 
            // detectedCornersRadioButton
            // 
            this.detectedCornersRadioButton.AutoSize = true;
            this.detectedCornersRadioButton.Location = new System.Drawing.Point(7, 227);
            this.detectedCornersRadioButton.Name = "detectedCornersRadioButton";
            this.detectedCornersRadioButton.Size = new System.Drawing.Size(108, 17);
            this.detectedCornersRadioButton.TabIndex = 0;
            this.detectedCornersRadioButton.TabStop = true;
            this.detectedCornersRadioButton.Text = "Detected Corners";
            this.detectedCornersRadioButton.UseVisualStyleBackColor = true;
            this.detectedCornersRadioButton.CheckedChanged += new System.EventHandler(this.detectedCornersRadioButton_CheckedChanged);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(896, 300);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Corners:";
            // 
            // listBox
            // 
            this.listBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listBox.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBox.FormattingEnabled = true;
            this.listBox.ItemHeight = 11;
            this.listBox.Location = new System.Drawing.Point(896, 316);
            this.listBox.Name = "listBox";
            this.listBox.Size = new System.Drawing.Size(141, 321);
            this.listBox.TabIndex = 6;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1046, 657);
            this.Controls.Add(this.listBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.optionsGroupBox);
            this.Controls.Add(this.harrisButton);
            this.Controls.Add(this.openButton);
            this.Controls.Add(this.pictureBox);
            this.Name = "Form1";
            this.Text = "T2 - Visão";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.optionsGroupBox.ResumeLayout(false);
            this.optionsGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Button openButton;
        private System.Windows.Forms.Button harrisButton;
        private System.Windows.Forms.GroupBox optionsGroupBox;
        private System.Windows.Forms.RadioButton squaredYDerivateRadioButton;
        private System.Windows.Forms.RadioButton squaredXDerivateRadioButton;
        private System.Windows.Forms.RadioButton cornerResponseRadioButton;
        private System.Windows.Forms.RadioButton detectedCornersRadioButton;
        private System.Windows.Forms.RadioButton smoothedDerivatesProductRadioButton;
        private System.Windows.Forms.RadioButton smoothedSquaredYDerivateRadioButton;
        private System.Windows.Forms.RadioButton smoothedSquaredXDerivateRadioButton;
        private System.Windows.Forms.RadioButton derivatesProductRadioButton;
        private System.Windows.Forms.RadioButton yDerivateRadioButton;
        private System.Windows.Forms.RadioButton xDerivateRadioButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox listBox;
    }
}

