﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace T2
{
    public partial class Form1 : Form
    {
        HarrisDialog harrisDialog = new HarrisDialog();
        float[,] I;
        Bitmap imageI, imageIx, imageIy, imageIxx, imageIyy, imageIxy, imageSxx, imageSyy, imageSxy, imageResponse;
        
        bool drawCorners = false;
        float bppip = 1;
        Point iCenter = Point.Empty, bCenter = Point.Empty;
        Pen pen = new Pen(Color.FromArgb(180, 155, 75));
        Corner[] corners = null;

        public Form1()
        {
            InitializeComponent();
        }

        private void UpdateDrawingData()
        {
            float imageAspect = ((float)imageI.Width) / ((float)imageI.Height);
            float boxAspect = ((float)pictureBox.Width) / ((float)pictureBox.Height);
            bppip = imageAspect <= boxAspect ?
                ((float)pictureBox.Height) / ((float)imageI.Height) :
                ((float)pictureBox.Width) / ((float)imageI.Width);
            bCenter = new Point(pictureBox.Width / 2, pictureBox.Height / 2);
            iCenter = new Point(imageI.Width / 2, imageI.Height / 2);
        }

        private void pictureBox_Resize(object sender, EventArgs e)
        {
            UpdateDrawingData();
        }

        private void openButton_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                Bitmap image = (Bitmap) Bitmap.FromFile(openFileDialog.FileName);
                I = new float[image.Width, image.Height];
                for (int i = 0; i < image.Width; i++)
                    for (int j = 0; j < image.Height; j++)
                        I[i, j] = image.GetPixel(i, j).GetBrightness();
                imageI = Processing.GetBitmapFromIntensity(I);
                pictureBox.Image = imageI;
                UpdateDrawingData();
                harrisButton.Enabled = true;

                detectedCornersRadioButton.Checked = cornerResponseRadioButton.Checked = xDerivateRadioButton.Checked = yDerivateRadioButton.Checked =
                    squaredXDerivateRadioButton.Checked = squaredYDerivateRadioButton.Checked = derivatesProductRadioButton.Checked =
                    smoothedSquaredXDerivateRadioButton.Checked = smoothedSquaredYDerivateRadioButton.Checked = smoothedDerivatesProductRadioButton.Checked = false;
                optionsGroupBox.Enabled = false;
                listBox.Items.Clear();
            }
        }

        private void harrisButton_Click(object sender, EventArgs e)
        {
            harrisDialog.DialogResult = DialogResult.Cancel;
            if (harrisDialog.ShowDialog() == DialogResult.OK)
            {
                float[,] Ix = Processing.Convolution(I, Processing.Gx);
                float[,] Iy = Processing.Convolution(I, Processing.Gy);
                float[,] Ixx = Processing.Product(Ix, Ix);
                float[,] Iyy = Processing.Product(Iy, Iy);
                float[,] Ixy = Processing.Product(Ix, Iy);
                float[,] G = Processing.GetGaussianKernel(3, harrisDialog.Covariance);
                float[,] Sxx = Processing.Convolution(Ixx, G);
                float[,] Syy = Processing.Convolution(Iyy, G);
                float[,] Sxy = Processing.Convolution(Ixy, G);
                float[,] response = Processing.CalculateResponse(Sxx, Sxy, Syy, harrisDialog.K);
                corners = Processing.SelectCorners(response, harrisDialog.Threshold, harrisDialog.Radius);

                imageIx = Processing.GetBitmapFromIntensity(Processing.Normalize(Ix));
                imageIy = Processing.GetBitmapFromIntensity(Processing.Normalize(Iy));
                imageIxx = Processing.GetBitmapFromIntensity(Processing.Normalize(Ixx));
                imageIyy = Processing.GetBitmapFromIntensity(Processing.Normalize(Iyy));
                imageIxy = Processing.GetBitmapFromIntensity(Processing.Normalize(Ixy));
                imageSxx = Processing.GetBitmapFromIntensity(Processing.Normalize(Sxx));
                imageSyy = Processing.GetBitmapFromIntensity(Processing.Normalize(Syy));
                imageSxy = Processing.GetBitmapFromIntensity(Processing.Normalize(Sxy));
                imageResponse = Processing.GetBitmapFromIntensity(Processing.Normalize(response));

                detectedCornersRadioButton.Checked = true;
                optionsGroupBox.Enabled = true;
                listBox.Items.Clear();
                foreach (var corner in corners)
                    listBox.Items.Add(corner);

                detectedCornersRadioButton.Checked = false;
                detectedCornersRadioButton.Checked = true;
            }
        }

        private void pictureBox_Paint(object sender, PaintEventArgs e)
        {
            if (drawCorners)
            {
                foreach (Corner corner in corners)
                {
                    Point p = new Point((int)(bCenter.X + bppip * (corner.Position.X - iCenter.X)), (int)(bCenter.Y + bppip * (corner.Position.Y - iCenter.Y)));
                    e.Graphics.DrawLine(pen, new Point(p.X - 6, p.Y), new Point(p.X + 6, p.Y));
                    e.Graphics.DrawLine(pen, new Point(p.X, p.Y - 6), new Point(p.X, p.Y + 6));
                }
            }
        }

        private void xDerivateRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (xDerivateRadioButton.Checked)
                pictureBox.Image = imageIx;
        }

        private void yDerivateRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (yDerivateRadioButton.Checked)
                pictureBox.Image = imageIy;
        }

        private void squaredXDerivateRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (squaredXDerivateRadioButton.Checked)
                pictureBox.Image = imageIxx;
        }

        private void squaredYDerivateRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (squaredYDerivateRadioButton.Checked)
                pictureBox.Image = imageIyy;
        }

        private void derivatesProductRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (derivatesProductRadioButton.Checked)
                pictureBox.Image = imageIxy;
        }

        private void smoothedSquaredXDerivateRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (smoothedSquaredXDerivateRadioButton.Checked)
                pictureBox.Image = imageSxx;
        }

        private void smoothedSquaredYDerivateRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (smoothedSquaredYDerivateRadioButton.Checked)
                pictureBox.Image = imageSyy;
        }

        private void smoothedDerivatesProductRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (smoothedDerivatesProductRadioButton.Checked)
                pictureBox.Image = imageSxy;
        }

        private void cornerResponseRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (cornerResponseRadioButton.Checked)
                pictureBox.Image = imageResponse;
        }

        private void detectedCornersRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (detectedCornersRadioButton.Checked)
            {
                drawCorners = true;
                pictureBox.Image = imageI;
            }
            else
                drawCorners = false;
        }
    }
}
